FROM alpine:3.16 AS builder
RUN apk update && apk add --no-cache curl vim wget bash nano tar


WORKDIR /tmp

RUN wget https://gitlab.com/lcslouis/vetdaysignup/-/archive/v.2025.1.2/vetdaysignup-v.2025.1.2.tar.gz
RUN tar xfvz vetdaysignup-v.2025.1.2.tar.gz
RUN mv vetdaysignup-v.2025.1.2 vetdaysignup


FROM php:7.3-apache



RUN apt update && apt install -y git libzip-dev zip libldap2-dev gnupg2 unixodbc-dev wget  && docker-php-ext-install zip mysqli ldap
# apache to load index.htm/php as default landing page
RUN echo "DirectoryIndex index.htm index.php"|tee -a /etc/apache2/apache2.conf
RUN echo "date.timezone=America/Chicago">/usr/local/etc/php/conf.d/CST-timezone.ini

#RUN wget http://192.168.1.7/gitlab-instance-4d2060ce/vetdaysignup/-/archive/2023.1.0/vetdaysignup-2023.1.0.tar.gz


COPY --chown=www-data:www-data --from=builder /tmp/vetdaysignup/ /var/www/html/
#RUN echo 64.225.58.175 vdpeasy.lcsworld.com > /etc/hosts; cat /etc/hosts
#RUN cat /etc/hosts